;;; -*- mode: emacs-lisp; coding: utf-8; indent-tabs-mode: nil; -*-

;;; Code:
(lazyload (magit-status) "magit")

(provide '020-magit)
